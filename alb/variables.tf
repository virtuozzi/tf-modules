variable "namespace" {
  type = string
}
variable "lb_name" {
  type = string
}
variable "vpc" {
  type = any
}
variable "sg" {
  type = any
}
variable "domain_name" {
  type = any
}
variable "tls_policy_on_lb" {
  type = string
}
variable "public_subnets" {}
variable "lb_settings" {
  type = map(any)
  default = {
    "httpd_redirect_enabled" = "true",
    "backend_port"           = "80",
    "backend_protocol"       = "HTTP",
    "backend_path"           = "/mypage",
    "healthcheck_port"       = "traffic-port",
  }
}
