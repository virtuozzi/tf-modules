data "aws_acm_certificate" "wildcard" {
  domain   = "*.${var.domain_name}"
  statuses = ["ISSUED"]
}
data "aws_route53_zone" "dns-zone" {
  name         = var.domain_name
  private_zone = false
}

resource "aws_route53_record" "record" {
  for_each = var.lb_settings
  zone_id  = data.aws_route53_zone.dns-zone.zone_id
  name     = "${each.value.dns_record}.${var.domain_name}"
  type     = "A"

  alias {
    name                   = aws_lb.lb.dns_name
    zone_id                = aws_lb.lb.zone_id
    evaluate_target_health = true
  }
}
resource "aws_lb" "lb" {
  name               = var.lb_name
  internal           = false
  load_balancer_type = "application"
  subnets            = var.public_subnets
  security_groups    = var.sg
}
resource "aws_lb_target_group" "group" {
  for_each             = var.lb_settings
  name_prefix          = each.value.tg_prefix
  port                 = each.value.backend_port
  protocol             = each.value.backend_protocol
  deregistration_delay = 60
  vpc_id               = var.vpc

  health_check {
    interval            = 300
    timeout             = 120
    unhealthy_threshold = 10
    healthy_threshold   = 5
    port                = each.value.healthcheck_port
    path                = each.value.backend_path
  }
  stickiness {
    enabled         = "true"
    type            = "lb_cookie"
    cookie_duration = "7200"
  }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = var.tls_policy_on_lb
  certificate_arn   = data.aws_acm_certificate.wildcard.arn
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content"
      status_code  = "200"
    }
  }
}
resource "aws_lb_listener_rule" "static" {
  for_each     = var.lb_settings
  listener_arn = aws_lb_listener.front_end.arn
  priority     = each.value.priority

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.group["${each.key}"].arn
  }

  condition {
    host_header {
      values = ["${each.value.dns_record}.${var.domain_name}"]
    }
  }
}
resource "aws_lb_listener" "redirect" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
