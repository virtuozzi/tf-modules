output "urls" {
  value = { for vm in keys(var.lb_settings) : vm => aws_route53_record.record[vm].name }
}
output "target_group_arn" {
  value = { for vm in keys(var.lb_settings) : vm => aws_lb_target_group.group[vm].arn }
}
