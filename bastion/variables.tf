variable "namespace" {
  type = string
}
variable "instance_type" {
  type = string
}
variable "public_subnets" {
  type = any
}
variable "security_group_id" {
  type = list(any)
}
variable "ssh_keypair" {
  type = string
}
variable "instance_count" {
  type = number
}
variable "ami_id" {
  type = string
}
variable "target_group_arns" {
  type    = list(string)
  default = []
}
variable "domain_name" {
  type = any
}
