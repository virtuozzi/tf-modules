output "public_ip" {
  value = data.aws_instances.instances.public_ips
}
output "dns_record" {
  value = aws_route53_record.bastion.name
}
