data "aws_region" "current" {}
data "aws_route53_zone" "dns-zone" {
  name         = var.domain_name
  private_zone = false
}
data "aws_instances" "instances" {
  depends_on = [aws_autoscaling_group.server]
  instance_tags = {
    ResourceGroup = var.namespace
    Name          = "${var.namespace}-bastion.${var.domain_name}"
  }

  instance_state_names = ["running", "pending"]
}

resource "aws_route53_record" "bastion" {
  zone_id = data.aws_route53_zone.dns-zone.zone_id
  name    = "bastion.${var.domain_name}"
  type    = "A"
  ttl     = 600
  records = data.aws_instances.instances.public_ips
}
resource "aws_launch_template" "server" {
  name_prefix   = "${var.namespace}-bastion.${var.domain_name}"
  image_id      = var.ami_id
  instance_type = var.instance_type
  block_device_mappings {
    device_name = "/dev/sda1"

    ebs {
      volume_size = 40
    }
  }
  user_data = filebase64("${path.module}/templates/startup.sh")
  key_name  = var.ssh_keypair
  network_interfaces {
    associate_public_ip_address = true
    security_groups             = var.security_group_id
    delete_on_termination       = true
  }
  tags = {
    Name = "${var.namespace}-bastion.${var.domain_name}"
  }
}
resource "aws_autoscaling_group" "server" {
  name                      = "${var.namespace}-bastion.${var.domain_name}"
  health_check_grace_period = 300
  health_check_type         = "ELB"
  target_group_arns         = var.target_group_arns
  default_cooldown          = 3600
  min_size                  = var.instance_count
  max_size                  = var.instance_count
  vpc_zone_identifier       = var.public_subnets
  launch_template {
    id      = aws_launch_template.server.id
    version = aws_launch_template.server.latest_version
  }
  tags = [
    {
      key                 = "ResourceGroup"
      value               = var.namespace
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = "${var.namespace}-bastion.${var.domain_name}"
      propagate_at_launch = true
    }
  ]
}
