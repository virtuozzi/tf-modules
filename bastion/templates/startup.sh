#!/usr/bin/env bash
set -e

function installDependencies() {
  echo "Installing dependencies..."
  sudo apt-get -qq update &>/dev/null
  sudo apt-get -yqq install unzip git make binutils nfs-common nmap&>/dev/null
  git clone https://github.com/aws/efs-utils && cd efs-utils
  ./build-deb.sh
  sudo apt install -yqq ./build/amazon-efs-utils*deb
}

function updateOS() {
  echo "Updating the OS"
  sudo apt-get update -qq &>/dev/null
  sudo apt-get upgrade -yqq &>/dev/null
}

function installDocker() {
  echo "Installing Docker..."
  curl -fsSL https://get.docker.com -o get-docker.sh
  sudo sh get-docker.sh
}


# Install software
installDependencies
installDocker

# Update the OS if needed
sleep 20
updateOS
