variable "namespace" {
  type = string
}
variable "sg" {
  type = any
}
variable "efs_mount" {
  type = map(map(any))
  default = {
    "gitlab" = {
      "path"        = "var-opt",
      "owner_gid"   = 0,
      "owner_uid"   = 0,
      "permissions" = 750,
    },
    "jenkins" = {
      "path"        = "etc",
      "owner_gid"   = 1000,
      "owner_uid"   = 1000,
      "permissions" = 750,
    }
  }
}
variable "azs" {
  type = any
}
variable "subnet_ids" {
  type = any
}
variable "mounts_enabled" {
  type    = bool
  default = true
}
variable "backup_tag" {
  type = string
}
