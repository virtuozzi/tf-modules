output "efs_file_system_arn" {
  value = aws_efs_file_system.this.arn
}
output "efs_file_system_id" {
  value = aws_efs_file_system.this.id
}
output "accesspoint_id" {
  value = { for accesspoint in keys(var.efs_mount) : accesspoint => aws_efs_access_point.access-point[accesspoint].id }
}
