resource "aws_efs_file_system" "this" {
  creation_token = var.namespace
  encrypted      = true

  tags = {
    Name   = var.namespace
    Backup = var.backup_tag
  }
}
resource "aws_efs_mount_target" "mount" {
  for_each       = var.mounts_enabled ? var.subnet_ids : []
  file_system_id = aws_efs_file_system.this.id
  subnet_id      = each.value
  security_groups = [
    var.sg,
  ]
}
resource "aws_efs_access_point" "access-point" {
  for_each       = var.efs_mount
  file_system_id = aws_efs_file_system.this.id
  posix_user {
    gid = each.value.owner_gid
    uid = each.value.owner_uid
  }
  root_directory {
    path = "/${each.key}"
    creation_info {
      owner_gid   = each.value.owner_gid
      owner_uid   = each.value.owner_uid
      permissions = each.value.permissions
    }
  }

  tags = {
    Name = "${each.key}"
  }
}
